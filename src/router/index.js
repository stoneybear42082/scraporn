import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store";

// import routes
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/portal-connection",
    name: "portal-Connection",
    component: () => import("@/views/Portal.vue")
  },
  {
    path: "/",
    name: "home",
    component: Home,
    meta: { requiresAuth: true },
    children: [
      {
        path: "",
        name: "home-dashboard",
        component: () => import("@/views/Home/Dashboard.vue")
      },
      {
        path: "scraping",
        name: "home-scraping",
        component: () => import("@/views/Home/Scraping.vue")
      },
      {
        path: "profil",
        name: "home-profil",
        component: () => import("@/views/Home/Profil.vue")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const userIsLogged = store.getters.getUserAuth;
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!userIsLogged) {
      next({
        name: "portal-Connection",
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
